<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anio;
	private $circula;

	//declaracion de setter para circula
	public function set_circula($circ){
		$this->circula = $circ;
	}

	//declaracion del método verificación
	public function verificacion(){
		if ($this->anio < 1990){
			return "No";
		}elseif ($this->anio >= 1990 && $this->anio <= 2010) {
			return "Revisión";
		}else{
			return "Si";
		}
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anio=$_POST['anio'];
	$Carro1->set_circula($Carro1->verificacion());
}
?>



