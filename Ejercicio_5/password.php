<?php  
//declaracion de clase password
	class password{
		//declaracion de atributos
		private $nombre;
		private $password;
		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->password= substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4);
		}

		public function mensaje(){
			return 'Hola '.$this->nombre.' has creado una nueva contraseña';
		}

		//declaracion de metodo destructor para mostrar password
		public function __destruct(){
			//destruye password
			$this->password='El password <b>'.$this->password.'</b> ha sido destruido';
			echo $this->password;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$pass1= new password($_POST['nombre']);
	$mensaje= $pass1->mensaje();
}


?>