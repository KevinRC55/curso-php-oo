<?php

/*class Carro{
    //declaracion de propiedades
    public $color;
    
}*/
require_once('carro.php');

//crea aqui la clase Moto junto con dos propiedades public
/*class Moto{
    public $marca;
    public $modelo;
}*/
require_once('moto.php');


//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidor='';


//crea aqui la instancia o el objeto de la clase Moto
$Moto1 = new Moto;

$Carro1 = new Carro;

 if ( !empty($_POST)){

    //almacenamos el valor mandado por POST en el atributo color
    $Carro1->color=$_POST['color'];
    //se construye el mensaje que sera lanzado por el servidor
    //$mensajeServidor='el servidor dice que ya escogiste un color: '.$_POST['color'];

    // recibe aqui los valores mandados por post
    $Moto1->marca = $_POST['marca'];
    $Moto1->modelo = $_POST['modelo'];
    $mensajeServidor='El servidor dice que ya escogiste una marca: '.$Moto1->marca.' y un modelo: '.$Moto1->modelo.' para la moto.';



 }  

?>

<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/bootstrap-grid.css">
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
    <title>
        Indice
    </title>
</head>
<body>
    
    <input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>

    <!-- aqui puedes insertar el mesaje del servidor para Moto-->


    <div class="container" style="margin-top: 4em">
    
    <header> <h1>Carro y Moto</h1></header><br>
    <form method="post">
        <div class="form-group row">

            <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
            <div class="col-sm-4">
                <input class="form-control" type="color" name="color" id="CajaTexto1">
            </div>

            <div class="col-sm-4">
            </div>

            <!-- inserta aqui los inputs para recibir los atributos del objeto-->
            <label class="col-sm-3" for="CajaTexto2">Marca de la moto:</label>
            <div class="col-sm-4">
                <input class="form-control" type="text" name="marca" id="CajaTexto2">
            </div>

            <div class="col-sm-4">
            </div>

            <label class="col-sm-3" for="CajaTexto3">Modelo de la moto:</label>
            <div class="col-sm-4">
                <input class="form-control" type="text" name="modelo" id="CajaTexto3">
            </div>

                        
        </div>
        <button class="btn btn-primary" type="submit" >enviar</button>
    </form>

    </div>


</body>
</html>