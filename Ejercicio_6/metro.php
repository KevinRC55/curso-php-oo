<?php 
	include_once('transporte.php');
	
	//declaracion de la clase hijo o subclase metro
	class metro extends transporte{
		private $numero_carros;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$car){
			parent::__construct($nom,$vel,$com);
			$this->numero_carros=$car;
		}

		// sobreescritura de metodo
		public function resumenMetro(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Número de carros:</td>
						<td>'. $this->numero_carros.'</td>				
					</tr>';
			return $mensaje;
		}
	}

	$mensaje='';


	if (!empty($_POST)){
		if ($_POST['tipo_transporte']=='subterraneo') {
			$metro1= new metro('Metro','70','na','7');
			$mensaje=$metro1->resumenMetro();
		}
	}
?>